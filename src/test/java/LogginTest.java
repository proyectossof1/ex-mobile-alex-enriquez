import io.appium.java_client.AppiumBy;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileCommand;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import javax.print.DocFlavor;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

public class LogginTest {
    //static AppiumDriver driverAppium;

    public static void main (String[] args) throws MalformedURLException, InterruptedException {
        //Llamando al escenario 1
        openApp();
    }

    public static void openApp() throws MalformedURLException, InterruptedException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("deviceName","Pixel 4 API 30(R)" );
        cap.setCapability("udid","emulator-5554" );
        cap.setCapability("platformName","Android" );
        cap.setCapability("platformVersion","11" );
        cap.setCapability("automationName","UiAutomator2" );
        cap.setCapability("appPackage","com.swaglabsmobileapp" );
        cap.setCapability("appActivity",".MainActivity" );
        AndroidDriver driver = new AndroidDriver(new URL("http://127.0.0.1:4723"), cap);
        Thread.sleep(4000);

        System.out.println("Automatización de Aplicativo Mobile Iniciada");
        System.out.println("Escenario 1 Loging Exitoso");

        //Declaración de variables con rutas apuntando al aplicativo pantalla 1
        WebElement user = driver.findElement(AppiumBy.accessibilityId("test-Username"));
        WebElement password = driver.findElement(AppiumBy.accessibilityId("test-Password"));
        WebElement loging = driver.findElement(By.xpath("\t\n" +
                "//android.view.ViewGroup[@content-desc=\"test-LOGIN\"]/android.widget.TextView"));

        //Envío de comandos
        user.sendKeys("standard_user");
        System.out.println("CORRECTO, Usuario ingresado con exito");
        password.sendKeys("secret_sauce");
        System.out.println("CORRECTO, Contraseña ingresada con exito");
        loging.click();
        System.out.println("CORRECTO, Inicio de sesión loging exitoso");

        Thread.sleep(4000);
        //Declaración de variables con rutas apuntando al aplicativo pantalla 2
        WebElement products = driver.findElement(By.xpath("\t\n" +
                "//android.view.ViewGroup[@content-desc=\"test-Cart drop zone\"]/android.view.ViewGroup/android.widget.TextView"));
        WebElement item = driver.findElement(By.xpath("\t\n" +
                "(//android.widget.TextView[@content-desc=\"test-Item title\"])[1]"));

        if (Objects.equals(products.getText(), "PRODUCTS")) {
            System.out.println("CORRECTO, el título PRODUCTOS si aparece en la pantalla");
        } else {
            System.out.println("ERROR, el título PRODUCTOS no aparece en la pantalla");
        }

        if (Objects.equals(item.getText(), "Sauce Labs Backpack")) {
            System.out.println("CORRECTO, el ítem Sauce Labs Backpack si aparece en la pantalla");
        } else {
            System.out.println("ERROR, el ítem Sauce Labs Backpack no aparece en la pantalla");
        }

        //Termina sesión de Appium
        driver.quit();

    }
}
